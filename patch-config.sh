#!/usr/bin/bash

CONFIG_PATH="$(realpath "$(dirname $0)/config.d")"

pushd "$1"

for i in arch/*/configs/*_defconfig
do
  rm .config -f
  ./scripts/kconfig/merge_config.sh -m "$i" "${CONFIG_PATH}"/*
  mv .config "$i"
  git add "$i"
done

git commit -m "Applied configs"

